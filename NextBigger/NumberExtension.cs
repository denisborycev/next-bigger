﻿using System;

namespace NextBiggerTask
{
    public static class NumberExtension
    {
        /// <summary>
        /// Finds the nearest largest integer consisting of the digits of the given positive integer number and null if no such number exists.
        /// </summary>
        /// <param name="number">Source number.</param>
        /// <returns>
        /// The nearest largest integer consisting of the digits  of the given positive integer and null if no such number exists.
        /// </returns>
        /// <exception cref="ArgumentException">Thrown when source number is less than 0.</exception>
        public static int? NextBiggerThan(int number)
        {
            int temp = number;
            if (temp >= 10 && temp != int.MaxValue)
            {
                    if (temp % 10 > (temp / 10) % 10)
                    {
                        return number + (((temp % 10) * 10) + ((temp / 10) % 10) - (temp % 100));
                    }
            }

            if (temp < 0)
            {
                throw new ArgumentException("Value of number cannot be less zero.");
            }

            return null;
        }
    }
}
